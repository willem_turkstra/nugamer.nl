<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
  protected $guarded = [];

  public function feed_items(){
    return $this->hasMany('App\Feed_item');
  }
}
