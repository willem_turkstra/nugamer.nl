<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      $schedule->call('\App\Http\Controllers\Backend\FeedItemCronController@run_cron', array(2))->everyThirtyMinutes();
      //$schedule->call('\App\Http\Controllers\SitemapController@cron', array('dragle', 2))->daily();
      $schedule->call('\App\Http\Controllers\Backend\FacebookController@post_daily_trending_feed_item')->dailyAt('20:00');
    }
}
