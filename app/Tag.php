<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    public function synonyms(){
      return $this->hasMany('App\Synonym');
    }

    public function categories(){
      return $this->belongsToMany('App\Category');
    }
}
