<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Website;

use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
			View::composer('layouts.app', 'App\Composers\AdminMenuComposer');
      View::composer('frontend.site', 'App\Composers\MenuComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
