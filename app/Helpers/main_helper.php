<?php
function get_meta_properties($link){
	$page_content = file_get_contents($link);
	libxml_use_internal_errors(true);
	$dom_obj = new \DOMDocument();
	$dom_obj->loadHTML($page_content);

	$data = array();

	foreach($dom_obj->getElementsByTagName('meta') as $meta) {
		$tag_id = $meta->getAttribute('property');
		if(empty($tag_id)){
			$tag_id = $meta->getAttribute('name');
		}
		if(empty($tag_id)){
			continue;
		}

		switch($tag_id){
			case "og:title":
			case "twitter:title":
				$data['title'] = $meta->getAttribute('content');
			break;
			case "og:description":
			case "twitter:description":
				$data['description'] = $meta->getAttribute('content');
			break;
			case "og:image":
			case "twitter:image":
			case "twitter:image:src":
				$data['image'] = $meta->getAttribute('content');
			break;
		}
	}

	return $data;
}
?>
