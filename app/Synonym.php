<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Synonym extends Model
{
    protected $table = 'synonyms';
    protected $guarded = [];

    public function tag(){
      return $this->belongsTo('App\Tag');
    }
}
