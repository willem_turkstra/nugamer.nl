<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Seo;

class Category extends Model
{
    protected $guarded = [];

    public function tags(){
      return $this->belongsToMany('App\Tag');
    }

    public function feed_items(){
      return $this->belongsToMany('App\Feed_item');
    }

    public function seo_relation(){
      return $this->morphOne('App\Seo', 'object');
    }

    public function seo(){
      $output = $this->seo_relation()->first();
      return !empty($output) ? $output : new Seo();
    }

    public function website(){
      return $this->belongsTo('App\Website');

    }
}
