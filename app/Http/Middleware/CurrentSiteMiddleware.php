<?php

namespace App\Http\Middleware;

use Closure;
use Session;

use App\Website;


class CurrentSiteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $website = Website::whereSlug($request->site)->first();
      if($website != null){
        Session::set('frontend_website', $website);
      }

      return $next($request);
    }
}
