<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use App\Tag;
use App\Seo;
use Session;
use Input;
use Redirect;
use Image;

class CategoryController extends BackendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(!Session::has('website'))
      {
        return Redirect::route('websites.index')->with('message', 'No website found!');
      }
      $website = Session::get('website');
      $categories = Category::where('website_id', '=', $website->id)->get();

      return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $category = new Category();
      return view('categories.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = Input::all();

      if(!Session::has('website'))
      {
        return Redirect::route('admin.websites.index')->with('message', 'No website found!');
      }
      $website = Session::get('website');
      $input['website_id'] = $website->id;

      $input['image'] = ($this->handle_category_image($request)) ? $file_name : '';
      if(empty($input['image'])){
        unset($input['image']);
      }

      $tags = array();
      if(!empty($input['tags'])){
        $tags = $input['tags'];
        unset($input['tags']);
      }
      $category = Category::create( $input );
      $category->tags()->sync($tags);

      return Redirect::route('admin.categories.index')->with('message', 'Website created');
    }

    function handle_category_image(Request $request){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $file_name = time() . '.' . $image->getClientOriginalExtension();
        $image = Image::make($image);
        $image->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $image->save(public_path('/uploads/categories/' . $file_name));
        return $file_name;
      }
      return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
      return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
      $input = array_except(Input::all(), '_method');

      $seo = $input['seo'];
      unset($input['seo']);

      if(!empty($input['tags'])){
        $category->tags()->sync($input['tags']);
        unset($input['tags']);
      }
      else{
        $category->tags()->sync([]);
      }

      $input['image'] = ($file_name = $this->handle_category_image($request)) ? $file_name  : '';
      if(empty($input['image'])){
        unset($input['image']);
      }

      $category->update($input);

      if($category->seo_relation()->first() == null){
        $category->seo_relation()->create($seo);
      }
      else {
        $category->seo_relation()->update($seo);
      }

      return Redirect::route('admin.categories.index')->with('message', 'Category updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
      $category->delete();
      return Redirect::route('admin.categories.index')->with('message', 'Category deleted.');
    }
}
