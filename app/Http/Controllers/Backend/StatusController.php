<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class StatusController extends BackendController
{
  function __construct(){
    $this->middleware('auth');
  }

  function set_object_state($table, $id, $state){
    DB::table($table)
    ->where('id', $id)
    ->update(array('status' => $state));
  }
}
