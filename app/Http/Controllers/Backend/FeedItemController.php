<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Feed_item;
use Session;
use Redirect;
use Input;
use App\Category;

class FeedItemController extends BackendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      if(!Session::has('website'))
      {
        return Redirect::route('admin.websites.index')->with('message', 'No website found!');
      }
      $website = Session::get('website');
      $input = Input::all();

      $feeditems = Feed_item::where('website_id', '=', $website->id);
      $search = '';
      if(!empty($input['search'])){
        $search = $input['search'];
        $feeditems->search($search);
      }
      $feeditems->orderBy('pub_date', 'DESC');
      $feeditems = $feeditems->paginate(12);

      return view('feeditems.index', compact('feeditems', 'search'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed_item $feed_item)
    {
      if(!Session::has('website'))
      {
        return Redirect::route('admin.websites.index')->with('message', 'No website found!');
      }
      $website = Session::get('website');
      $categories_from_db = Category::whereWebsiteId($website->id)->get()->toArray();

      $categories = array();
      foreach($categories_from_db as $category){
        $categories[$category['id']] = $category['title'];
      }

      $selected_categories = array();
      foreach($feed_item->categories as $category){
        $selected_categories[$category->id] = $category->title;
      }


      return view('feeditems.edit', compact('feed_item', 'categories', 'selected_categories'));
    }

    public function update(Feed_item $feed_item){
      $input = Input::all();
      $feed_item->categories()->sync($input['selected_categories']);

      return Redirect::route('admin.feeditems.index')->with('message', 'Feeditem updated');
    }
}
