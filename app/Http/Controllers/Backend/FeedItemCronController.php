<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Website;
use App\Category;
use App\Feed;
use App\Feed_item;
use App\Feed_item_meta_data;
use App\Synonym;
use Session;
use Feeds;
use Image;

class FeedItemCronController extends BackendController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function run_cron($website_id){
      if(empty($website_id)){
        die('No Website Id Given!');
      }
      $website = Website::whereId($website_id)->first();
      if($website == null){
        die('No Website Found With Given Id!');
      }

      //Load all feeds for current website
      $feeds = Feed::whereWebsiteId($website->id)->where('status', 'A')->get();

      foreach($feeds as $feed){
        $this->load_feed_items($feed, $website->id);
      }
      exit;
    }
//DELETE cfi FROM category_feed_item cfi, category_feed_item cfi2 WHERE cfi.feed_item_id > cfi2.feed_item_id AND cfi.category_id = cfi2.category_id AND cfi.feed_item_id <> cfi2.feed_item_id

  /*  public function link_feed_items_to_categories($website_id){
      if(empty($website_id)){
        die('No Website Id Given!');
      }
      $website = Website::whereId($website_id)->first();
      if($website == null){
        die('No Website Found With Given Id!');
      }

      //Load all feeds for current website
      $feed_items = Feed_item::whereWebsiteId($website->id)->get();
      $synonyms = Synonym::lists('id', 'title')->all();

      foreach($feed_items as $feeditem){
        $linked_to_category = false;
        foreach ($synonyms as $synonym => $synonym_id) {
          if (preg_match('/\b' . $synonym . '\b/i', $feeditem->description) || preg_match('/\b' . $synonym . '\b/i', $feeditem->title)) {
            $synonym_obj = Synonym::where('title', $synonym)->first();

            if($synonym_obj != null){
              $categories = $synonym_obj->tag()->first()->categories()->get();
              foreach($categories as $category){
                if(!$category->feed_items->contains($feeditem->id)){
                  $category->feed_items()->attach($feeditem);
                  $linked_to_category = true;
                }
              }
            }
          }
        }
        if(!$linked_to_category){
          $other_category = Category::where('title', 'Other')->where('website_id', $website_id)->first();
          if($other_category != null){
            if(!$category->feed_items->contains($feeditem->id)){
              $other_category->feed_items()->attach($feeditem);
            }
          }
        }
      }
    }*/

    protected function load_feed_items(Feed $feed, $website_id){
      $data = Feeds::make($feed->url);
      $items = $data->get_items();

      $added_feed_items_guids = array();
      $synonyms = Synonym::lists('id', 'title')->all();

      foreach($items as $feed_item){
        $existing_feeditem = Feed_item::where('guid', $feed_item->get_id())->first();
        if($existing_feeditem != null){
          continue;
        }

        $existing_feeditem = Feed_item::where('link', $feed_item->get_link())->first();
        if($existing_feeditem != null){
          continue;
        }


        $added_feed_items_guids[] = $feed_item->get_id();


        $feeditem = new Feed_item();
        $feeditem->guid = $feed_item->get_id();
        $feeditem->title = $feed_item->get_title();
        $feeditem->description = $feed_item->get_content();
        $feeditem->link = $feed_item->get_link();
        $feeditem->website_id = $website_id;
        $feeditem->feed_id = $feed->id;
        $feeditem->pub_date = $feed_item->get_date('Y-m-d H:i:s');
        $feeditem->status = 'A';

        if(!empty($feed_item->get_author())){
          $feeditem->author = $feed_item->get_author();
        }

        $feeditem->save();

        $this->link_feed_items_to_categories($synonyms, $feeditem, $website_id);
        if($this->find_and_create_meta_data_for_feed_item($feeditem)){

          if($feeditem->meta_data->keyBy('key')['image']->value != null){
            $this->get_feed_item_image_and_save($feeditem, $feeditem->meta_data->keyBy('key')['image']->value);
          }
        }
      }
    }

    protected function find_and_create_meta_data_for_feed_item($feeditem){
      $meta_data = get_meta_properties($feeditem->link);
      if($meta_data == null){
        return false;
      }

      foreach($meta_data as $key => $value){
        $data = array(
          'feed_item_id' => $feeditem->id,
          'key' => $key,
          'value' => $value
        );
        if($key == 'image'){
          if(!empty($value)){
            $image_data = getimagesize($value);
            if($image_data[0] > 2000){
              continue;
            }
            if($image_data[1] > 2000){
              continue;
            }
          }
        }
        Feed_item_meta_data::create($data);
      }
      return true;
    }

    protected function link_feed_items_to_categories($synonyms, $feeditem, $website_id){
      $linked_to_category = false;
      foreach ($synonyms as $synonym => $synonym_id) {
        if (preg_match('/\b' . $synonym . '\b/i', $feeditem->description) || preg_match('/\b' . $synonym . '\b/i', $feeditem->title)) {
          $synonym_obj = Synonym::where('title', $synonym)->first();

          if($synonym_obj != null){
            $categories = $synonym_obj->tag->categories()->get();
            foreach($categories as $category){
              if(!$category->feed_items->contains($feeditem->id)){
                $category->feed_items()->attach($feeditem);
                $linked_to_category = true;
              }
            }
          }
        }
      }
      if(!$linked_to_category){
        $other_category = Category::where('title', 'Other')->where('website_id', $website_id)->first();
        if($other_category != null){
          if(!$other_category->feed_items->contains($feeditem->id)){
            $other_category->feed_items()->attach($feeditem);
          }
        }
      }
    }

    protected function get_feed_item_image_and_save($feeditem, $image_url){
      if(!$this->does_image_exists($image_url)){
        return false;
      }

      $thumb_image = Image::make($image_url);
      $thumb_image->resize(400, null, function ($constraint) {
          $constraint->aspectRatio();
      });

      $file_name = basename($image_url);
      $thumb_path = 'feed_items/thumbs/' . $file_name;
      $thumb_image->save(public_path($thumb_path));

      $image = Image::make($image_url);
      $file_path = 'feed_items/' . $file_name;
      $image->save(public_path($file_path));

      $feeditem->thumb_image = $thumb_path;
      $feeditem->image = $file_path;
      $feeditem->save();
    }

    public function does_image_exists($url){
      $handle = curl_init($url);
      curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

      /* Get the HTML or whatever is linked in $url. */
      $response = curl_exec($handle);

      /* Check for 404 (file not found). */
      $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
      curl_close($handle);
      if($httpCode == 404) {
        return false;
      }
      return true;
    }

    public function fix_images(){
      $feed_items = Feed_item::where('image', '')->get();

      foreach($feed_items as $feed_item){
        if(!empty($feed_item->meta_data->keyBy('key')['image'])){
          $image_url = $feed_item->meta_data->keyBy('key')['image']->value;

          if(!$this->does_image_exists($image_url)){
            continue;
          }

          $thumb_image = Image::make($image_url);
          $thumb_image->resize(400, null, function ($constraint) {
              $constraint->aspectRatio();
          });

          $file_name = basename($image_url);
          $thumb_path = 'feed_items/thumbs/' . $file_name;
          $thumb_image->save(public_path($thumb_path));

          $image = Image::make($image_url);
          $file_path = 'feed_items/' . $file_name;
          $image->save(public_path($file_path));

          $feed_item->thumb_image = $thumb_path;
          $feed_item->image = $file_path;
          $feed_item->save();
        }
      }
      exit;
    }

}
