<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Feed;
use App\Website;
use Session;
use Redirect;
use Input;

class FeedController extends BackendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(!Session::has('website'))
      {
        return Redirect::route('websites.index')->with('message', 'No website found!');
      }
      $website = Session::get('website');
      $feeds = Feed::where('website_id', '=', $website->id)->get();

      return view('feeds.index', compact('feeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $feed = new Feed();
      return view('feeds.create', compact('feed'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = Input::all();

      if(!Session::has('website'))
      {
        return Redirect::route('admin.websites.index')->with('message', 'No website found!');
      }
      $website = Session::get('website');
      $input['website_id'] = $website->id;

      $feed = Feed::create( $input );

      return Redirect::route('admin.feeds.index')->with('message', 'Feed created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
      return view('feeds.edit', compact('feed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feed $feed)
    {
      $input = array_except(Input::all(), '_method');

      $feed->update($input);

      return Redirect::route('admin.feeds.index')->with('message', 'Feed updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feed $feed)
    {
      $feed->delete();
      return Redirect::route('admin.feeds.index')->with('message', 'Feed deleted.');
    }
}
