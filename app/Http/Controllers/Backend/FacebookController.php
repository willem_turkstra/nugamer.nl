<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Feed_item;
use DB;


class FacebookController extends BackendController
{
    private $_page_id = '345895149082406';
    private $_page_access_token = 'EAACEdEose0cBAC3po1r0BJsQsz3YeHF955LLZCkWMHZBPqZBqjngy6wTOkwVLO0NZC8V7l0BFV3nZApVzxrP1skZCchZBMuYfsmIq0scXN6OoIz9vdhZC6Uh3gzS1ZBkcwuyzZA0QsZB8KNZAGWZC4cQvVwLvLWC8I0dp06KNxYBPi3uXdyfBxpUZAEUm3';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function post_daily_trending_feed_item(){
      $fb = App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
      $feed_item = Feed_item::select(DB::raw('*, SUM(feed_item_popularities.popularity) as sum_popularity'))
        ->leftJoin('feed_item_popularities', function($join){
          $join->on('feed_item_popularities.feed_item_id', '=', 'feed_items.id');
        })//*->where(function($query){
          // $query->whereRaw('feed_item_popularities.date = CURDATE()')->orWhere('feed_item_popularities.date', null);
        //})
        ->where('feed_items.status', 'A')
        ->groupBy('feed_items.id')
        ->having('sum_popularity', '>', 0)
//          ->orderBy('feed_item_popularities.updated_at', 'DESC')
        ->orderBy('feed_item_popularities.date', 'DESC')
        ->orderBy('sum_popularity', 'DESC')
        ->first();


      $url = $feed_item->categories->first()->website->url . '/item/' .$feed_item->id. '/' . str_slug($feed_item->title, "-");

      $fb->post('/345895149082406/feed', ['message' => 'Top bericht van de dag', 'link' => $url], $this->_page_access_token);
    }
}
