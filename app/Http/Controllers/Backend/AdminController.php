<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Feed_item;
use App\Feed;
use App\Feed_item_popularity;
use DB;
use Session;

class AdminController extends BackendController
{
    function __construct(){
      $this->middleware('auth');
    }

    function index(){

      if(!Session::has('website'))
      {
        return view('404');
      }
      $website = Session::get('website');

      $feed_item_count = Feed_item::where('website_id', $website->id)->count();
      $items_viewed = Feed_item_popularity::where('popularity', '10')->count();
      $websites_visited = Feed_item_popularity::where('popularity', '50')->count();
      $newest_feed_item = Feed_item::orderBy('pub_date', 'DESC')->where('website_id', $website->id)->first();
      $hotest_feed_item = $this->get_hottest_feed_item();

      return view('home', compact('feed_item_count', 'items_viewed', 'websites_visited', 'newest_feed_item', 'hotest_feed_item', 'feed_item_graph_data'));
    }

    function get_hottest_feed_item(){
        return Feed_item::select(DB::raw('*, SUM(feed_item_popularities.popularity) as sum_popularity'))
        ->leftJoin('feed_item_popularities', function($join){
          $join->on('feed_item_popularities.feed_item_id', '=', 'feed_items.id');
        })->where(function($query){
          $query->whereRaw('feed_item_popularities.date = CURDATE()')->orWhere('feed_item_popularities.date', null);
        })->where('feed_items.status', 'A')->orderBy('sum_popularity', 'DESC')
        ->groupBy('feed_items.id')
        ->orderBy('feed_item_popularities.updated_at', 'DESC')
        ->orderBy('feed_items.pub_date', 'DESC')->first();
    }
}



/*
*/
