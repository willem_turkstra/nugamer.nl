<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Session;

class GraphController extends BackendController
{
    function __construct(){
      $this->middleware('auth');
    }

    function feed_item_count(){
      $week = date('W');
      $year = date('Y');

      $result = $this->get_weekly_feed_data_by_week_id($year, $week);
      $result1 = $this->get_weekly_feed_data_by_week_id($year, $week-1);

      $result = json_decode(json_encode($result), true);
      $result1 = json_decode(json_encode($result1), true);
      $data_array = array_column($result, 'feed_item_count');
      $date_array = array_column($result, 'date');
      $data_prev_week_array = array_column($result1, 'feed_item_count');

      return array($date_array, $data_array, $data_prev_week_array);
    }

    function get_weekly_feed_data_by_week_id($year, $week){
      if(!Session::has('website')){
        return array();
      }
      $website = Session::get('website');

      return DB::select("
      SELECT
          COUNT(id) as feed_item_count,
          days.date
      FROM
      (
        select DAY((STR_TO_DATE('". $year . $week ." SUNDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." SUNDAY', '%X%V %W')) as date union
        select DAY((STR_TO_DATE('". $year . $week ." MONDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." MONDAY', '%X%V %W')) as date union
        select DAY((STR_TO_DATE('". $year . $week ." TUESDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." TUESDAY', '%X%V %W')) as date union
        select DAY((STR_TO_DATE('". $year . $week ." WEDNESDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." WEDNESDAY', '%X%V %W')) as date union
        select DAY((STR_TO_DATE('". $year . $week ." THURSDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." THURSDAY', '%X%V %W')) as date union
        select DAY((STR_TO_DATE('". $year . $week ." FRIDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." FRIDAY', '%X%V %W')) as date union
        select DAY((STR_TO_DATE('". $year . $week ." SATURDAY', '%X%V %W'))) as open_day, (STR_TO_DATE('". $year . $week ." SATURDAY', '%X%V %W')) as date
      ) days
      LEFT JOIN (
        SELECT id, DAY(created_at) as openday
          FROM `feed_items`
        WHERE MONTH(created_at) = MONTH(CURRENT_TIMESTAMP)
        AND website_id = " . $website->id . ") feed_items on days.open_day = feed_items.openday
      GROUP BY open_day");

    }
}
