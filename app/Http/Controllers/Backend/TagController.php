<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tag;
use App\Synonym;
use Session;
use Input;
use Redirect;
use DB;

class TagController extends BackendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tags = Tag::get();

      return view('tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tag = new Tag();
        return view('tags.create', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = Input::all();

      $synonyms = $input['synonym'];
      unset($input['synonym']);

      $tag = Tag::create( $input );

      foreach($synonyms as $synonym){
        $synonym['tag_id'] = $tag->id;
        Synonym::create($synonym);
      }

      return Redirect::route('admin.tags.index')->with('message', 'Tag created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
          return view('tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
      $input = array_except(Input::all(), '_method');


      $synonyms = $input['synonym'];
      unset($input['synonym']);

      $tag->update($input);


      foreach($synonyms as $id => $synonym_data){
        if(!empty($synonym_data['id'])){
          $synonym = Synonym::where('id', $synonym_data['id'])->first();
          if(!is_null($synonym)){
            $synonym->title = $synonym_data['title'];
            $synonym->save();
          }
        }
        else {
          $synonym = new Synonym();
          $synonym->title = $synonym_data['title'];
          $synonym->tag_id = $tag->id;
          $synonym->save();
        }
      }

      return Redirect::route('admin.tags.index')->with('message', 'Tag updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
      $tag->delete();
      return Redirect::route('admin.tags.index')->with('message', 'Tag deleted.');
    }

    public function ajax_get_synonym_form(){
      return view('tags.partials._synonym');
    }

    public function ajax_get_tags(){
      $tags = DB::table('tags')->where('status', 'A')->select('id', 'title as text')->get();
      return $tags;
    }
}
