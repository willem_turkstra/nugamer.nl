<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Synonym;

class TagSynonymController extends BackendController
{
    public function destroy($id){
      $synonym = Synonym::where('id', $id)->first();
      $synonym->delete();
    }
}
