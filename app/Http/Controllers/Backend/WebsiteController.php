<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Website;
use Input;
use Redirect;
use Session;

class WebsiteController extends BackendController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $websites = Website::all();
        return view('websites.index', compact('websites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $website = new Website();
        return view('websites.create', compact('website'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        Website::create( $input );

        if(!Session::has('website'))
        {
          $website = Website::where('status', '=', 'A')->first();
          if($website){
            Session::set('website', $website);
          }

        }

        return Redirect::route('admin.websites.index')->with('message', 'Website created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Website $website)
    {
        return view('websites.edit', compact('website'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Website $website)
    {
      //$this->validate($request, $this->rules);

      $input = array_except(Input::all(), '_method');
      $website->update($input);

      return Redirect::route('admin.websites.index', $website->id)->with('message', 'Website updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Website $website)
    {
      $current_website = Session::get('website');
      if(isset($current_website)){
        if($current_website->id == $website->id){
          $new_current_website = Website::where('status', '=', 'A')->where('id', '!=', $website->id)->first();
          Session::set('website', $new_current_website);
        }
      }

      $website->delete();
      return Redirect::route('admin.websites.index')->with('message', 'Website deleted.');
    }


    public function select($id){
      $website = Website::find($id);
      Session::set('website', $website);
      return redirect()->back();
    }
}
