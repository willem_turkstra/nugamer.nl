<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Artisan;

use App\Http\Requests;

class ArtisanController extends BackendController
{
    function handler(Request $request, $command){
      switch($command){
        case "migrate":
          echo "Starting migration";
          Artisan::call('migrate');
          echo "Migration done";
        break;
        case "migrate-rollback":
          echo "Starting migration rollback";
          Artisan::call('migrate:rollback');
          echo "Migration rollback done";
        break;
        case "vendor-publish":
        echo "Starting vendor publish";
        Artisan::call('vendor:publish');
        echo "Vendor publish done";
        break;
        case "cache":
          echo "Clearing cache";
          echo "<br />";
          Artisan::call('cache:clear');
          echo "Cache cleared";
        break;
        default:
          echo "No command found";
          exit;
      }

      if($request->has('return')){
        $return = $request->return;
        if($return == 'back'){
          return back()->with('Success', 'Cache has been cleared!');
        }
        else {
          return redirect($return);

        }
      }

    }
}
