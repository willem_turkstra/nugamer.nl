<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use App\Feed_item;

use Session;
use Cache;
use SEOMeta;
use OpenGraph;
use URL;

class CategoryController extends FrontendController
{
    public function index(Request $request, $slug, $category){
      $website = Session::get('frontend_website');

      $slug = $category;

      $category = Category::where('slug', $category)->where('website_id', $website->id)->first();
      if($category == null){
          return view('404');
      }
      $website = Session::get('frontend_website');

      $feed_items = Feed_item::where('website_id', $website->id)->where('status', 'A')->whereHas('categories', function($q) use ($category) {
          $q->where('category_id', $category->id);
      })->orderBy('pub_date', 'DESC')->paginate(16);

      if($category != null){
        SEOMeta::setTitle($category->seo()->page_title);
        SEOMeta::setDescription($category->seo()->meta_description);
        SEOMeta::setKeywords($category->seo()->meta_keywords);
        SEOMeta::setCanonical($request->url());
        OpenGraph::setTitle($category->seo()->page_title);
        OpenGraph::addImage(URL::to('/') . '/' . $category->image);
        OpenGraph::setDescription($category->seo()->meta_description);


        return view('frontend.category.index', compact('category', 'feed_items'));
      }
      return view('404');
    }
}
