<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Feed_item;
use Session;
use Input;


class SearchController extends FrontendController
{
    public function search(){
      $website = Session::get('frontend_website');
      $input = Input::all();

      $feed_items = Feed_item::where('website_id', '=', $website->id);
      $search = '';
      if(!empty($input['search'])){
        $search = $input['search'];
        $feed_items->search($search);
      }
      else {
        return redirect()->back();
      }
      $feed_items->orderBy('pub_date', 'DESC');
      $feed_items = $feed_items->paginate(12);

      return view('frontend.search.results', compact('feed_items', 'search'));
    }
}
