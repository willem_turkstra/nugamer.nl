<?php
/*SELECT * FROM `feed_items` fi
LEFT JOIN feed_item_popularities fip ON fip.feed_item_id = fi.id
WHERE (fip.date = CURDATE() OR fip.date IS null)
ORDER BY fip.popularity DESC, fip.updated_at DESC, fi.pub_date DESC*/

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use App\Feed_item;

use DB;
use Cache;

class HomeController extends \App\Http\Controllers\Controller
{
    public function index(){
      if(!empty($_GET['type']) && $_GET['type'] == 'trending'){
        return  $this->get_trending_feed_items();
      }
      else {
        return $this->get_latests_feed_items();
      }
    }

    protected function get_latests_feed_items(){
      return Cache::remember('homepage_latest', 30, function(){
        $website = Session::get('frontend_website');
        $feed_items = Feed_item::where('website_id', $website->id)->where('status', 'A')->orderBy('pub_date', 'DESC')->limit(32)->get();
        return view('frontend.home', array('feed_items' => $feed_items))->render();
      });
    }

    protected function get_trending_feed_items(){
      //return Cache::remember('homepage_trending', 30, function(){
        $website = Session::get('frontend_website');
        $feed_items = Feed_item::select(DB::raw('*, SUM(feed_item_popularities.popularity) as sum_popularity'))
          ->leftJoin('feed_item_popularities', function($join){
            $join->on('feed_item_popularities.feed_item_id', '=', 'feed_items.id');
          })//*->where(function($query){
            // $query->whereRaw('feed_item_popularities.date = CURDATE()')->orWhere('feed_item_popularities.date', null);
          //})
          ->where('feed_items.status', 'A')
          ->groupBy('feed_items.id')
          ->having('sum_popularity', '>', 0)
//          ->orderBy('feed_item_popularities.updated_at', 'DESC')
          ->orderBy('feed_item_popularities.date', 'DESC')
          ->orderBy('sum_popularity', 'DESC')
          ->limit(32)->get();

        return view('frontend.home', array('feed_items' => $feed_items));
      //});
    }
}
