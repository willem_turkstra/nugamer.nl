<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;

class FrontendController extends \App\Http\Controllers\Controller
{
  public function __contruct(){
    if(!Session::has('frontend_website')){
      return view('404');
    }
  }
}
