<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Feed_item;
use App\Feed_item_popularity;
use Session;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Redirect;
use SEOMeta;
use OpenGraph;
use URL;

class FeedItemController extends FrontendController
{
    public function index(Request $request, $slug, $id, $item_name = ''){
      $website = Session::get('frontend_website');

      $item = Feed_item::where('website_id', $website->id)->where('id', $id)->first();
      if($item == null){
        return view('404');
      }
      Session::set('breadcrumb_category', $item->categories->first());

      $this->feed_item_popularity_add($request, $id);

      SEOMeta::setTitle($item->title);
      SEOMeta::setCanonical($request->url());
      OpenGraph::setTitle($item->title);
      OpenGraph::addImage(URL::to('/') . '/' . $item->image);

      return view('frontend.item.view', compact('item'));
    }

    public function feed_item_popularity_add(Request $request, $id, $value = 10){

      $CrawlerDetect = new CrawlerDetect;
      if($CrawlerDetect->isCrawler()){
        return false;
      }

      $pop_record = feed_item_popularity::where('client_ip', $request->ip())
      ->where('feed_item_id', $id)
      ->where('date', date('Y-m-d'))
      ->where('popularity', $value)
      ->first();
      if ($pop_record !== null) {
        return true;
      }
      $data = array();
      $data['feed_item_id'] = $id;
      $data['popularity'] = $value;
      $data['client_ip'] = $request->ip();
      $data['date'] = date('Y-m-d');

      feed_item_popularity::create($data);
      return true;
    }

    public function leave(Request $request, $slug, $id){
      $website = Session::get('frontend_website');
      $item = Feed_item::where('website_id', $website->id)->where('id', $id)->first();
      if($item == null){
        return view('404');
      }
      $this->feed_item_popularity_add($request, $id, 50);

      return Redirect::away($item->link . '/?ref=dragle');
    }
}
