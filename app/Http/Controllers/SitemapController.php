<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use URL;
use App\Feed_item;
use App\Category;
use App\Website;

class SitemapController extends Controller
{

    function manuel($site){
      $website = Session::get('website');
      if($site == $website->slug){
        $this->generate_sitemap($site, $website->id);
      }
    }

    function cron($site, $website_id){
      $this->generate_sitemap($site, $website_id);
    }

    function generate_sitemap($site_name, $website_id){
      $sitemap = \App::make("sitemap");

      $website = Website::find($website_id);
      if($website == false){
        return false;
      }

      $categories = Category::where('website_id', $website_id)->where('status', 'A')->get();
      foreach($categories as $category){
        $sitemap->add(URL::to('/'. $category->slug), $category->updated_at, '0.5');
      }

      $feed_items = Feed_item::where('website_id', $website_id)->where('status', 'A')->get();
      foreach ($feed_items as $item){
        $sitemap->add($website->url . '/item/' . $item->id . '/' . str_slug($item->title, "-"), $item->updated_at, '0.5');
      }


      // generate your sitemap (format, filename)
      $sitemap->store('xml', 'sitemap_'. $site_name);
    }

}
