<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::bind('websites', function($value, $route) {
	return App\Website::whereId($value)->first();
});

Route::bind('categories', function($value, $route) {
	return App\Category::whereId($value)->first();
});

Route::bind('tags', function($value, $route) {
	return App\Tag::whereId($value)->first();
});
Route::bind('synonyms', function($value, $route) {
	return App\Synonym::whereId($value)->first();
});

Route::bind('feeds', function($value, $route) {
	return App\Feed::whereId($value)->first();
});
Route::bind('feeditems', function($value, $route) {
	return App\Feed_item::whereId($value)->first();
});

Route::auth();
Route::group(['prefix' => 'admin', 'namespace' => 'Backend'], function()
{

	Route::get('/', 'AdminController@index');
	Route::get('artisan/{command}', 'ArtisanController@handler');
	Route::get('graph/feed_count', 'GraphController@feed_item_count');

	Route::resource('websites', 'WebsiteController');
	Route::get('websites/select/{id}', 'WebsiteController@select');

	Route::resource('categories', 'CategoryController');


	Route::get('/tags/ajax_get_synonym_form', 'TagController@ajax_get_synonym_form');
	Route::get('/tags/ajax_get_tags', 'TagController@ajax_get_tags');
	Route::resource('tags', 'TagController');
	Route::delete('/synonyms/{id}', 'TagSynonymController@destroy');

	Route::resource('feeds', 'FeedController');
	Route::resource('feeditems', 'FeedItemController');
	Route::post('feeditems/search', [
		'as' => 'admin.feeditems.search',
		'uses' => 'FeedItemController@search'
	]);
	Route::get('feeditems/reset_search', [
	  'as' => 'admin.feeditems.reset_search',
		'uses' => 'FeedItemController@reset_search'
	]);

	Route::get('fix_images_feed_items', 'FeedItemCronController@fix_images');
	Route::get('daily_fb_post', 'FacebookController@post_daily_trending_feed_item');
	Route::get('{table}/{id}/{status}',  'StatusController@set_object_state');
});
Route::get('cron/feed/items/{website_id}', 'Backend\FeedItemCronController@run_cron');


Route::get('/admin/sitemap/{site}', 'SitemapController@manuel');

Route::group(['domain' => '{site}.nl', 'middleware' => 'currentsite', 'namespace' => 'Frontend'], function()
{
	Route::get('/', 'HomeController@index');
	Route::get('/search', 'SearchController@search');
	Route::get('/{slug}', 'CategoryController@index');
	Route::get('/item/{id}', 'FeedItemController@index');
	Route::get('/item/{id}/leave', 'FeedItemController@leave');
	Route::get('/item/{id}/{item_name}', 'FeedItemController@index');

});
