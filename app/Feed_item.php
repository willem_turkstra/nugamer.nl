<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Feed_item extends Model
{
    use Eloquence;

    protected $guarded = [];
    protected $searchableColumns = ['title', 'description'];



    protected $dates = ['created_at', 'updated_at', 'pub_date'];

    public function meta_data(){
      return $this->hasMany('App\Feed_item_meta_data');
    }

    public function categories(){
      return $this->belongsToMany('App\Category');
    }

    public function feed(){
      return $this->belongsTo('App\Feed');
    }

    public function feed_item_popularities(){
      return $this->hasMany('App\Feed_item_popularity');
    }
}
