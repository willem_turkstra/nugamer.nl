<?php
namespace App\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Users\Repository as UserRepository;

use App\Website;

class AdminMenuComposer {
    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $websites = Website::all();
        $view->with('websites', $websites);
    }

}
