<?php
namespace App\Composers;

use Illuminate\Contracts\View\View;

use App\Category;
use Session;
use Cache;

class MenuComposer {
    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
      if(Session::has('frontend_website')){
        return view('404');
      }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
      $website = Session::get('frontend_website');

			/*$categories = Cache::rememberForever('categories_' . $website->id, function() use ($website) {
        return Category::where('featured', 0)->where('status', 'A')->where('website_id', $website->id)->get();
      });*/
      $categories = Category::where('featured', 0)->where('status', 'A')->where('website_id', $website->id)->get();
      $view->with('categories', $categories);

      /*$featured_categories = Cache::rememberForever('featured_categories_' . $website->id, function() use ($website) {
         return Category::where('status', 'A')->where('featured', 1)->where('website_id', $website->id)->get();
      });*/
      $featured_categories = Category::where('status', 'A')->where('featured', 1)->where('website_id', $website->id)->get();
      $view->with('featured_categories', $featured_categories);
    }

}
