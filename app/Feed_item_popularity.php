<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed_item_popularity extends Model
{
		protected $primaryKey = 'feed_item_id';
    protected $guarded = [];

    public function feed_item(){
      return $this->belongsTo('App\Feed_item');
    }
}
