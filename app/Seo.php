<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
  protected $fillable = [
      'page_title', 'meta_description', 'meta_keywords'
  ];

  public function object(){
    return $this->morphTo();
  }
}
