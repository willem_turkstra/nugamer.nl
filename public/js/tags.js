

var Tag = {
	get_tags_url: "/admin/tags/ajax_get_tags",

	form: function(){
		var ajax_form_url = "/admin/tags/ajax_get_synonym_form";
		$.ajax({
			method: "GET",
			url: ajax_form_url
		})
		.done(function( data ) {
	    $('#synonym_form_container').append(data);
	  });
	},

	remove: function(){
		if($(this).data('tag-id')){
			var tag_id = $(this).data('tag-id');
			var ajax_form_url = "/admin/synonyms/" + tag_id;
			$.ajax({
				method: "DELETE",
				url: ajax_form_url
			})
			.done(function( data ) {});
		}
		//TODO: Clean up !!!
		$(this).parent().parent().parent().remove();
	},
	get: function() {
		$.ajax({
			method: "GET",
			url: Tag.get_tags_url
		}).done(function(data){
			console.log(data);
		});
	},
	select2Init: function(selector){
		$(selector).select2({
			ajax: {
		    url: Tag.get_tags_url,
		    dataType: 'json',
		    data: function (params) {
		      return {
		        q: params.term, // search term
		        page: params.page
		      };
		    },
				processResults: function (data) {
			    return {
			      results: data
			    };
				}
			}
  	});
	}
}
