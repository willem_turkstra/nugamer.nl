$(document).ready(function(){
	var ctx, myLineChart, options;
	Chart.defaults.global.responsive = true;
	ctx = $('#jumbotron-line-feed-item-chart').get(0).getContext('2d');
	options = {
	 showScale: false,
	 scaleShowGridLines: false,
	 scaleGridLineColor: "rgba(0,0,0,.05)",
	 scaleGridLineWidth: 0,
	 scaleShowHorizontalLines: false,
	 scaleShowVerticalLines: false,
	 bezierCurve: false,
	 bezierCurveTension: 0.4,
	 pointDot: false,
	 pointDotRadius: 0,
	 pointDotStrokeWidth: 2,
	 pointHitDetectionRadius: 20,
	 datasetStroke: true,
	 datasetStrokeWidth: 3,
	 datasetFill: true,
	 legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
 };

	$.ajax({
		method: "GET",
		url: "/admin/graph/feed_count"
	}).done(function(data){
		var line_data = {
			labels: data[0],
	    datasets: [
	      {
	        label: "My First dataset",
	        fillColor: "rgba(26, 188, 156,0.2)",
	        strokeColor: "#1ABC9C",
	        pointColor: "#1ABC9C",
	        pointStrokeColor: "#fff",
	        pointHighlightFill: "#fff",
	        pointHighlightStroke: "#1ABC9C",
	        data: data[1]
	      }, {
	        label: "My Second dataset",
	        fillColor: "rgba(34, 167, 240,0.2)",
	        strokeColor: "#22A7F0",
	        pointColor: "#22A7F0",
	        pointStrokeColor: "#fff",
	        pointHighlightFill: "#fff",
	        pointHighlightStroke: "#22A7F0",
	        data: data[2]
	      }
	    ]
		};
		myLineChart = new Chart(ctx).Line(line_data, options);
	});
});
