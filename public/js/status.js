$(document).ready(function(){
	$('.update-object-status').click(function(){
		var table = $(this).data('table');
		var id = $(this).data('id');
		var status = $(this).data('status');

		var result = Status.update_status(table, id, status);

		var a_top =$(this).parent().parent().parent().find('button span:first-child');
		if(status == 'A'){
			a_top.text('Active');
		}
		else {
			a_top.text('Disabled');
		}
	});
});

var Status = {
	update_status(table, id, status){
		$.ajax({
			method:"GET",
			url: "/admin/" + table + "/" + id + "/" + status
		}).done(function(data){});
	}
};
