<?php

use User;
use Website;

class UserTableSeeder extends Seeder {

    public function run()
    {
			User::create([
					'name' => 'Willem',
					'email' => 'willemturkstra@hotmail.com',
					'password' => bcrypt('letmein'),
			]);
			User::create([
					'name' => 'Thijs',
					'email' => 'thijs.schalk@gmail.com',
					'password' => bcrypt('letmein'),
			]);

			Website::create([
				'title' => 'Gaming',
				'url' => 'gaming.dragle.willemturkstra.nl',
				'slug' => 'gaming',
				'status' => 'A'
			]);
			Website::create([
				'title' => 'News',
				'url' => 'news.dragle.willemturkstra.nl',
				'slug' => 'news',
				'status' => 'A'
			]);
    }
}
