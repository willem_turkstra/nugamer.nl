<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePopularity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('feed_item_popularities', function(Blueprint $table){
        $table->integer('feed_item_id')->unsigned()->default(0);
        $table->integer('popularity')->default(0);
        $table->date('date');
        $table->foreign('feed_item_id')->references('id')->on('feed_items')->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('feed_item_popularities');
    }
}
