<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFeedItemCategoryLinkToManyToMany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('feed_items', function ($table) {
        $table->dropColumn('category_id');
      });

      Schema::create('category_feed_item', function(Blueprint $table){
        $table->integer('feed_item_id')->unsigned()->default(0);
        $table->integer('category_id')->unsigned()->default(0);
        $table->foreign('feed_item_id')->references('id')->on('feed_items')->onDelete('cascade');
        $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feed_items', function ($table) {
          $table->integer('category_id')->unsigned()->default(0);
        });
        Schema::drop('category_tag');
    }
}
