<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('seos', function (Blueprint $table) {
         $table->primary(array('object_id', 'object_type'));
         $table->morphs('object');
         $table->string('custom_heading');
         $table->string('page_title');
         $table->string('meta_description');
         $table->string('meta_keywords');
         $table->string('seo_name');
         $table->string('canonical_url');
         $table->timestamps();
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
     Schema::drop('seos');
   }
}
