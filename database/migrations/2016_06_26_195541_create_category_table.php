<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('categories', function(Blueprint $table){
         $table->increments('id');
         $table->string('title');
         $table->string('image');
         $table->string('description');
         $table->string('slug');
         $table->boolean('featured');
         $table->char('status', 1)->defaults('A');
         $table->integer('website_id')->unsigned()->default(0);
         $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('categories');
     }
}
