<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedFeedItemLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('feed_items', function ($table) {
        $table->integer('feed_id')->unsigned()->default(0);
        $table->foreign('feed_id')->references('id')->on('feeds')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('feed_items', function ($table) {
        $table->dropColumn('feed_id');
      });
    }
}
