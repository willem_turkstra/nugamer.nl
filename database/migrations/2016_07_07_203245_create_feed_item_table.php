<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedItemTable extends Migration
{
    /**
     * Run the migrations.
     * feed_item_id
     * title
     * description
     * link
     * created_timestamp
     * author
     * @return void
     */
    public function up()
    {
      Schema::create('feed_items', function(Blueprint $table){
        $table->increments('id');
        $table->string('title');
        $table->text('description');
        $table->string('link');
        $table->string('author');
        $table->integer('website_id')->unsigned()->default(0);
        $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feed_items');
    }
}
