<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feeds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('feeds', function(Blueprint $table){
        $table->increments('id');
        $table->string('title');
        $table->string('url');
        $table->string('format', 3)->defaults('xml');
        $table->char('status', 1)->defaults('A');
        $table->integer('website_id')->unsigned()->default(0);
        $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('feeds');
    }
}
