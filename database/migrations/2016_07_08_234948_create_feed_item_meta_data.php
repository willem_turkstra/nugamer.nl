<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedItemMetaData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('feed_item_meta_data', function(Blueprint $table){
        $table->integer('feed_item_id')->unsigned()->default(0);;
        $table->string('key');
        $table->text('value');
        $table->foreign('feed_item_id')->references('id')->on('feed_items')->onDelete('cascade');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feed_item_meta_data');
    }
}
