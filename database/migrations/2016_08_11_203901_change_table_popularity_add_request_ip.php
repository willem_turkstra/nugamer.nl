<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTablePopularityAddRequestIp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('feed_item_popularities', function(Blueprint $table){
        $table->string('client_ip');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('feed_item_popularities', function(Blueprint $table){
        $table->dropColumn('client_ip');
      });
    }
}
