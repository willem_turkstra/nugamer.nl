<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Feed_item;
use Intervention\Image\Facades\Image;

class UpdateFeedItemData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $feed_items = Feed_item::all();
        foreach($feed_items as $feed_item){
          if(!empty($feed_item->meta_data->keyBy('key')['image'])){
            $image_url = $feed_item->meta_data->keyBy('key')['image']->value;
            if(!$this->does_image_exists($image_url)){
              continue;
            }

            $thumb_image = Image::make($image_url);
            $thumb_image->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $file_name = basename($image_url);
            $thumb_path = 'feed_items/thumbs/' . $file_name;
            $thumb_image->save(public_path($thumb_path));

            $image = Image::make($image_url);
            $file_path = 'feed_items/' . $file_name;
            $image->save(public_path($file_path));

            $feed_item->thumb_image = $thumb_path;
            $feed_item->image = $file_path;
            $feed_item->save();
          }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function does_image_exists($url){
      $handle = curl_init($url);
      curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

      /* Get the HTML or whatever is linked in $url. */
      $response = curl_exec($handle);

      /* Check for 404 (file not found). */
      $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
      curl_close($handle);
      if($httpCode == 404) {
        return false;
      }
      return true;
    }
}
