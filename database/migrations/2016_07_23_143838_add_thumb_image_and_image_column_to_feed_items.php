<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThumbImageAndImageColumnToFeedItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('feed_items', function ($table) {
        $table->string('thumb_image');
        $table->string('image');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('feed_items', function ($table) {
        $table->dropColumn('thumb_image');
        $table->dropColumn('image');
      });
    }
}
