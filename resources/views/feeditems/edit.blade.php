@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
    <div class="card-header"><div class="card-title"><div class="title">Feed items</div></div></div>
      <div class="card-body">
        {!! Form::model($feed_item, ['method' => 'PATCH', 'route' => ['admin.feeditems.update', $feed_item->id], 'class' => 'form-horizontal']) !!}
            @include('feeditems/partials/_form', ['submit_text' => 'Update feed item'])
        {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
