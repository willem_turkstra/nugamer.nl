@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title">Feed items</div>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open(['method'=> 'GET', 'route' => ['admin.feeditems.index'], 'class' => 'form-horizontal']) !!}
          <div class="input-group">
            <input placeholder="Search..." type="text" class="form-control" name="search" value="{{$search}}"/>
            <div class="input-group-btn">
              <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
            </div>
          </div>
        {!! Form::close() !!}
        <table class="table table-striped">
          <thead>
            <th>Title</th>
            <th></th>
            <th></th>
          </thead>
          @foreach( $feeditems as $feeditem )
            <tr>
              <td>
                <a href="{{ $feeditem->url }}" target="blank">{{ $feeditem->title }}</a>
                <span class="sub-title company">{{ $feeditem->feed->title}}</span>
              </td>
              <td class="column-actions">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-gear"></i> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>{!! link_to_route('admin.feeditems.edit', 'Edit', array($feeditem->id)) !!}</li>
                    <!--<li>{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('admin.feeditems.destroy', $feeditem->id))) !!}
                           {!! Form::submit('Delete') !!}
                       {!! Form::close() !!}</li>-->
                  </ul>
                </div>
              </td>
              <td class="column-status">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span>
                    @if ($feeditem->status == 'A')
                      Active
                    @else
                      Disabled
                    @endif
                    </span>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                      <li><a class="update-object-status" data-table="feed_items" data-id="{{$feeditem->id}}" data-status="A">Active</a></li>
                      <li><a class="update-object-status" data-table="feed_items" data-id="{{$feeditem->id}}" data-status="D">Disabled</a></li>
                  </ul>
                </div>
              </td>
            </tr>
          @endforeach
        </table>
        {{ $feeditems->appends(Request::only('search'))->render() }}
			</div>
		</div>
	</div>
</div>
@endsection
