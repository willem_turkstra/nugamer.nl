{{ csrf_field() }}
<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
    <li role="presentation"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
  </ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="general">
			<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
					<label for="title" class="col-md-4 control-label">Title</label>
					<div class="col-md-6">
							<input id="title" type="text" class="form-control" name="title" value="{{ $feed_item->title }}" disabled>
							@if ($errors->has('title'))
									<span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group{{ $errors->has('afbeelding') ? ' has-error' : '' }}">
					<label for="description" class="col-md-4 control-label">Image</label>
					<div class="col-md-6">
							<img src="/{{$feed_item->image}}" width="500"/>
							@if ($errors->has('description'))
									<span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
					<label for="description" class="col-md-4 control-label">Description</label>
					<div class="col-md-6">
							<textarea id="description" class="form-control" name="description" disabled rows="8">{{ $feed_item->description }}</textarea>
							@if ($errors->has('description'))
									<span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
					<label for="link" class="col-md-4 control-label">Link</label>
					<div class="col-md-6">
							<a class="form-link" target="blank" name="link" href="{{ $feed_item->link }}">{{$feed_item->link}}</a>
							@if ($errors->has('link'))
									<span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
									</span>
							@endif
					</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="categories">
			<div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
					<label for="title" class="col-md-4 control-label">Categories</label>
					<div class="col-md-6">
							@foreach($categories as $category_id => $category)
              <div class="form-group">
                <div class="checkbox3 checkbox-inline checkbox-check checkbox-light">
                  <input type="checkbox" id="category-checkbox-{{$category_id}}" name="selected_categories[]" value="{{$category_id}}"
                  @if(!empty($selected_categories[$category_id])) checked="" @endif
                   />
                  <label for="category-checkbox-{{$category_id}}">
                    {{$category}}
                  </label>
                </div>
              </div>
							@endforeach
							@if ($errors->has('categories'))
									<span class="help-block">
											<strong>{{ $errors->first('categories') }}</strong>
									</span>
							@endif
					</div>
			</div>
		</div>
	</div>
<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
						<i class="fa fa-btn fa-save"></i> Save
				</button>
		</div>
</div>
