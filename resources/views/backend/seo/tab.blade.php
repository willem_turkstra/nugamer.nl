<div class="form-group">
	<label for="form_page_title" class="col-sm-2 control-label">@lang('common.page_title')</label>
	<div class="col-sm-10">
		<input id="form_page_title" class="form-control" placeholder="{{ $object->seo()->page_title }}" type="text" name="seo[page_title]" value="{{ $object->seo()->page_title }}" />
	</div>
</div>
<div class="form-group">
	<label for="form_meta_description" class="col-sm-2 control-label">@lang('common.meta_description')</label>
	<div class="col-sm-10">
		<input id="form_meta_description" class="form-control" placeholder="{{ substr(strip_tags($object->seo()->meta_description), 0, 155) }}" type="text" name="seo[meta_description]" value="{{ $object->seo()->meta_description }}" />
	</div>
</div>
<div class="form-group">
	<label for="form_meta_keywords" class="col-sm-2 control-label">@lang('common.meta_keywords')</label>
	<div class="col-sm-10">
		<input id="form_meta_keywords" class="form-control" type="text" name="seo[meta_keywords]" value="{{ $object->seo()->meta_keywords }}" />
	</div>
</div>
