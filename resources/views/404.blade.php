@extends('frontend.site')

@section('content')
<div class="container">
    <div class="row text-center">
      <div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-12">
				<h1>404</h1>
				<p>This page does not exist</p>
      </div>
    </div>
</div>
@endsection
