{{ csrf_field() }}
<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
    <li role="presentation"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">Seo</a></li>
  </ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="general">
			<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
					<label for="title" class="col-md-4 control-label">Title</label>
					<div class="col-md-6">
							<input id="title" type="text" class="form-control" name="title" value="{{ $category->title }}">
							@if ($errors->has('title'))
									<span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
					<label for="image" class="col-md-4 control-label">Image</label>
					<div class="col-md-6">
							<input type="file" name="image" class="form-control" id="image" />
							@if(!empty($category->image))
								<a data-toggle="modal" data-target="#category_image_modal">
									Preview
								</a>
							@endif
							@if ($errors->has('image'))
									<span class="help-block">
											<strong>{{ $errors->first('image') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="category_image_modal" tabindex="-1" role="dialog" aria-labelledby="Category Image">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Category Image</h4>
			      </div>
			      <div class="modal-body">
			        <img src="/uploads/categories/{{$category->image}}" />
			      </div>
			      <div class="modal-footer">
			      </div>
			    </div>
			  </div>
			</div>

			<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
					<label for="description" class="col-md-4 control-label">Description</label>
					<div class="col-md-6">
							<textarea name="description" class="form-control" id="description">{{$category->description}}</textarea>
							@if ($errors->has('description'))
									<span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
					<label for="slug" class="col-md-4 control-label">Slug</label>
					<div class="col-md-6">
							<input id="slug" type="text" class="form-control" name="slug" value="{{ $category->slug }}">
							@if ($errors->has('slug'))
									<span class="help-block">
											<strong>{{ $errors->first('slug') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">
					Featured
				</label>
				<div class="col-md-6 checkbox{{ $errors->has('featured') ? ' has-error' : '' }}">
						<label for="featured" class="control-label">
							<input type="checkbox" name="featured" value="1" @if ($category->featured == 1)checked @endif id="featured"/> Featured
						</label>
				</div>
			</div>
			<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
					<label for="status" class="col-md-4 control-label">Status</label>
					<div class="col-md-6">
							<select id="status" name="status" class="selectpicker">
								<option @if ($category->status == 'A')selected @endif>Active</option>
								<option @if ($category->status == 'D')selected @endif>Disabled</option>
							</select>
							@if ($errors->has('status'))
									<span class="help-block">
											<strong>{{ $errors->first('status') }}</strong>
									</span>
							@endif
					</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label">
					Tags
				</label>
				<div class="col-md-6">
					<select name="tags[]" class="tags-select2" multiple>
							@foreach($category->tags as $tag)
								<option selected value="{{$tag->id}}">{{$tag->title}}</option>
							@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-save"></i> Save
							</button>
					</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="seo">
		    @include('backend.seo.tab', ['object' => $category])
		</div>
	</div>
</div>
