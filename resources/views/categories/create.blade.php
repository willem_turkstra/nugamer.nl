@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
			<div class="card-header"><div class="card-title"><div class="title">Categories</div></div></div>
			<div class="card-body">
        {!! Form::model($category, ['route' => ['admin.categories.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
            @include('categories/partials/_form', ['submit_text' => 'Create category'])
        {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
