@extends('layouts.site')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <a class="article-card" href="detail.html">
          <img src="http://placehold.it/400x300">
          <div class="body">
            <div class="card-title">Game name (Could be Battlefield 1 or something else)</div>
            <div class="footer">
              <div class="posted pull pull-left"><i class="fa fa-clock-o"></i> x hours ago</div>
              <div class="company pull pull-right">ign <i class="fa fa-building"></i></div>
            </div>
          </div>
        </a>
      </div>
    </div>
</div>
@endsection
