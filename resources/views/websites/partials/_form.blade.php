{{ csrf_field() }}
<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		<label for="title" class="col-md-4 control-label">Title</label>
		<div class="col-md-6">
				<input id="title" type="text" class="form-control" name="title" value="{{ $website->title }}">
				@if ($errors->has('title'))
						<span class="help-block">
								<strong>{{ $errors->first('title') }}</strong>
						</span>
				@endif
		</div>
</div>
<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
		<label for="url" class="col-md-4 control-label">Url</label>
		<div class="col-md-6">
				<input id="url" type="url" class="form-control" name="url" value="{{ $website->url }}">
				@if ($errors->has('url'))
						<span class="help-block">
								<strong>{{ $errors->first('url') }}</strong>
						</span>
				@endif
		</div>
</div>
<div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
		<label for="slug" class="col-md-4 control-label">Subdomain slug</label>
		<div class="col-md-6">
				<input id="slug" type="text" class="form-control" name="slug" value="{{ $website->slug }}">
				@if ($errors->has('slug'))
						<span class="help-block">
								<strong>{{ $errors->first('slug') }}</strong>
						</span>
				@endif
		</div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
		<label for="status" class="col-md-4 control-label">Status</label>
		<div class="col-md-6">
				<select id="status" name="status" class="selectpicker">
					<option @if ($website->status == 'A')selected @endif>Active</option>
					<option @if ($website->status == 'D')selected @endif>Disabled</option>
				</select>
				@if ($errors->has('status'))
						<span class="help-block">
								<strong>{{ $errors->first('status') }}</strong>
						</span>
				@endif
		</div>
</div>
<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
						<i class="fa fa-btn fa-save"></i> Save
				</button>
		</div>
</div>
