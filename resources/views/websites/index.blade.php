@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
  	<div class="card">
			<div class="card-header"><div class="card-title"><div class="title">Websites</div></div></div>
			<div class="card-body">
				<div class="btn-group">
					<a href="{!! route('admin.websites.create') !!}" class="btn btn-success">Create website</a>
				</div>
				<table class="table table-striped">
          <thead>
            <th>Title</th>
            <th></th>
            <th></th>
          </thead>
          @foreach( $websites as $website )
            <tr>
              <td>
                <a href="{{ $website->url }}" target="blank">{{ $website->title }}</a>
              </td>
              <td class="column-actions">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-gear"></i> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>{!! link_to_route('admin.websites.edit', 'Edit', array($website->id)) !!}</li>
                    <li>{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('admin.websites.destroy', $website->id))) !!}
                           {!! Form::submit('Delete') !!}
                       {!! Form::close() !!}</li>
                  </ul>
                </div>
              </td>
              <td class="column-status">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span>
                    @if ($website->status == 'A')
                      Active
                    @else
                      Disabled
                    @endif
                    </span>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                      <li><a class="update-object-status" data-table="websites" data-id="{{$website->id}}" data-status="A">Active</a></li>
                      <li><a class="update-object-status" data-table="websites" data-id="{{$website->id}}" data-status="D">Disabled</a></li>
                  </ul>
                </div>
              </td>
            </tr>
          @endforeach
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
