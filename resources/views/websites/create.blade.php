@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
			<div class="card-header"><div class="card-title"><div class="title">Websites</div></div></div>
			<div class="card-body">
        {!! Form::model($website, ['route' => ['admin.websites.store'], 'class' => 'form-horizontal']) !!}
            @include('websites/partials/_form', ['submit_text' => 'Create website'])
        {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
