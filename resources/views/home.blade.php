@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <a href="#">
      <div class="card red summary-inline">
        <div class="card-body">
          <i class="icon fa fa-files-o fa-4x"></i>
          <div class="content">
            <div class="title">{{$feed_item_count}}</div>
            <div class="sub-title">Feed items</div>
          </div>
          <div class="clear-both"></div>
        </div>
      </div>
    </a>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <a href="#">
      <div class="card yellow summary-inline">
        <div class="card-body">
          <i class="icon fa fa-eye fa-4x"></i>
          <div class="content">
            <div class="title">{{ $items_viewed }}</div>
            <div class="sub-title">Items viewed</div>
          </div>
          <div class="clear-both"></div>
        </div>
      </div>
    </a>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <a href="#">
      <div class="card blue summary-inline">
        <div class="card-body">
          <i class="icon fa fa-globe fa-4x"></i>
          <div class="content">
            <div class="title">{{ $websites_visited }}</div>
            <div class="sub-title">Websites visited</div>
          </div>
          <div class="clear-both"></div>
        </div>
      </div>
    </a>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <a href="#">
      <div class="card green summary-inline">
        <div class="card-body">
          <i class="icon fa fa-mouse-pointer fa-4x"></i>
          <div class="content">
            <div class="title">{{ round($websites_visited / $items_viewed * 100) }} %</div>
            <div class="sub-title">Click through rate</div>
          </div>
          <div class="clear-both"></div>
        </div>
      </div>
    </a>
  </div>
</div>
<div class="row  no-margin-bottom">
  <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="card card-success article">
          <div class="card-header"><div class="card-title"><div class="title">Hottest item</div></div></div>
          <div class="card-body no-padding">
            <div class="thumbnail no-margin-bottom">
              <div class="image" style="background-image: url({{ $hotest_feed_item->meta_data->keyBy('key')['image']->value}})"></div>
              <div class="caption">
                <h3 id="thumbnail-label"><a href="/item/{{$hotest_feed_item->id}}">{{$hotest_feed_item->title}}</a></h3>
                <div class="footer">
          				<div class="posted pull pull-left"><i class="fa fa-clock-o"></i> {{$hotest_feed_item->pub_date->diffForHumans() }}</div>
          				<div class="company pull pull-right">{{$hotest_feed_item->feed->title }} <i class="fa fa-building"></i></div>
          			</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="card card-success article">
          <div class="card-header"><div class="card-title"><div class="title">Newest item</div></div></div>
          <div class="card-body no-padding">
            <div class="thumbnail no-margin-bottom">
              <div class="image" style="background-image: url({{ $newest_feed_item->meta_data->keyBy('key')['image']->value}})"></div>
              <div class="caption">
                <h3 id="thumbnail-label"><a href="/item/{{$newest_feed_item->id}}">{{$newest_feed_item->title}}</a></h3>
                <div class="footer">
          				<div class="posted pull pull-left"><i class="fa fa-clock-o"></i> {{$newest_feed_item->pub_date->diffForHumans() }}</div>
          				<div class="company pull pull-right">{{$newest_feed_item->feed->title }} <i class="fa fa-building"></i></div>
          			</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3 col-sm-12 col-xs-12">
    <div class="card primary">
      <div class="card-jumbotron no-padding">
        <canvas id="jumbotron-line-feed-item-chart" class="chart no-padding"></canvas>
      </div>
      <div class="card-body half-padding">
        <h4 class="float-left no-margin font-weight-300">Feed items gathered</h4>
        <div class="clear-both"></div>
      </div>
    </div>
  </div>
</div>
@endsection
