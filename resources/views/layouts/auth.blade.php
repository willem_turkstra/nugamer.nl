<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dragle Web Manager</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    {!!Html::style('css/select2/select2.min.css')!!}
    {!!Html::style('css/admin.css')!!}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    {!! HTML::style('css/themes/flat-blue.css') !!}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    {!!Html::script('js/config.js')!!}
	</head>
	<body class="flat-blue">
    <div class="app-container">
	  	@if (Session::has('message'))
	    <div class="container">
	      <div class="row">
	        <div class="col-md-10 col-md-offset-1">
	          <div class="alert alert-info">
	            <p>{{ Session::get('message') }}</p>
	          </div>
	        </div>
	      </div>
	    </div>
	    @endif
      <!-- Main Content -->
      <div class="container-fluid">
        <div class="side-body padding-top">
          @yield('content')
        </div>
      </div>
    </div>
    <footer class="app-footer">
      <div class="wrapper">
        <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright.
      </div>
    </footer>
    
		<!-- JavaScripts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
		{!!Html::script('js/select2/select2.full.min.js')!!}
		{!!Html::script('js/tags.js')!!}
		{!!Html::script('js/tags.page.js')!!}
		{!!Html::script('js/categories.page.js')!!}
		{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
		<!-- Javascript -->
		{!!Html::script('js/app.js')!!}
		{!!Html::script('js/index.js')!!}
	</body>
</html>
