<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dragle Web Manager</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    {!!Html::style('css/select2/select2.min.css')!!}
    {!!Html::style('css/admin.css')!!}
    {!!Html::style('css/admin-extra.css')!!}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    {!! HTML::style('css/themes/flat-blue.css') !!}
    {!! HTML::style('css/checkbox3/checkbox3.min.css') !!}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    {!!Html::script('js/config.js')!!}
  </head>
  <body class="flat-blue">
    <div class="app-container">
      <div class="row content-container">
        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-expand-toggle">
                <i class="fa fa-bars icon"></i>
              </button>
              <ol class="breadcrumb navbar-breadcrumb">
                <li class="active">Dashboard</li>
              </ol>
              <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                <i class="fa fa-th icon"></i>
              </button>
            </div>
            <ul class="nav navbar-nav navbar-right">
              <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                <i class="fa fa-times icon"></i>
              </button>
              @if (!Auth::guest())
                @if (Session::has('website'))
                <li class="dropdown profile">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                      <div class="profile-info">
                        <h4 class="username">{{ Auth::user()->name }} </h4>
                        <p>{{ Auth::user()->email }} </p>
                        <div class="btn-group margin-bottom-2x" role="group">
                          <a class="btn btn-default" href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                        </div>
                      </div>
                    </li>
                  </ul>
                </li>
                @endif
                @if (Session::has('website'))
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                      {{ Session::get('website')->title }} <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                      @foreach($websites as $website)
                      <li><a href="/admin/websites/select/{{$website->id}}">{{$website->title}}</a></li>
                      @endforeach
                  </ul>
                </li>
                @endif
              @endif
            </ul>
          </div>
        </nav>
        <div class="side-menu sidebar-inverse">
          <nav class="navbar navbar-default" role="navigation">
            <div class="side-menu-container">
              <div class="navbar-header">
                <a class="navbar-brand" href="/" target="blank">
                  <div class="icon fa fa-paper-plane"></div>
                  <div class="title">Dragle Web Manager</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                  <i class="fa fa-times icon"></i>
                </button>
              </div>
              <ul class="nav navbar-nav">
                <li>
                  <a href="{{ url('/admin') }}">
                    <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                  </a>
                </li>
                @if (!Auth::guest())
                <li>
                  <a href="{{ url('admin/websites') }}">
                    <span class="icon fa fa-globe"></span><span class="title">Websites</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('admin/feeds') }}">
                    <span class="icon fa fa-link"></span><span class="title">Feeds</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('admin/feeditems') }}">
                    <span class="icon fa fa-files-o"></span><span class="title">Feed items</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('admin/categories') }}">
                    <span class="icon fa fa-sitemap"></span><span class="title">Categories</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('admin/tags') }}">
                    <span class="icon fa fa-tags"></span><span class="title">Tags</span>
                  </a>
                </li>
                @endif
              </ul>
            </div>
            <!-- /.navbar-collapse -->
          </nav>
        </div>
        @if (Session::has('message'))
          <div class="notification alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p>{{ Session::get('message') }}</p>
          </div>
        </div>
        @endif
        <!-- Main Content -->
        <div class="container-fluid">
          <div class="side-body padding-top">
            @yield('content')
          </div>
        </div>
      </div>
      <footer class="app-footer">
        <div class="wrapper">
          <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright.
        </div>
      </footer>
    </div>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
    {!!Html::script('js/select2/select2.full.min.js')!!}
    {!!Html::script('js/chartjs/charts.min.js')!!}

    {!!Html::script('js/tags.js')!!}
    {!!Html::script('js/status.js')!!}
    {!!Html::script('js/tags.page.js')!!}
    {!!Html::script('js/categories.page.js')!!}
    {!!Html::script('js/config.js')!!}
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <!-- Javascript -->
    {!!Html::script('js/app.js')!!}
    {!!Html::script('js/dashboard.js')!!}
  </body>
</html>
