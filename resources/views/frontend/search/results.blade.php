@extends('frontend.site')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="article-detail">
				<ol class="breadcrumb lonely">
					<li><a href="/">Home</a></li>
					<li>Search</li>
				</ol>
			</div>
		</div>
	</div>
	<div class="row">
		@foreach($feed_items as $item)
			@include('frontend/components/grid_feed_item')
		@endforeach
	</div>
	<div class="row">
		<div class="col-md-12">
			{{ $feed_items->appends(Request::only('search'))->render() }}
		</div>
	</div>
</div>
@endsection
