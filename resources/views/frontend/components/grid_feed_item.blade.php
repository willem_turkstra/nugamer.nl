<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
	<a class="article-card" href="/item/{{$item->id}}/{{str_slug($item->title, "-")}}">
		@if(!empty($item->thumb_image))
		<div class="article-image" style="background-image: url({{ $item->thumb_image}});"></div>
		@else
		<img class="article-image" style="background-image: url(http://placehold.it/400x300)" />
		@endif
		<div class="body">
			<div class="card-title">{{{ $item->title }}}</div>
			<div class="footer">
				<div class="posted pull pull-left"><i class="fa fa-clock-o"></i> {{$item->pub_date->diffForHumans() }}</div>
				<div class="company pull pull-right">{{$item->feed->title }} <i class="fa fa-building"></i></div>
			</div>
		</div>
	</a>
</div>
