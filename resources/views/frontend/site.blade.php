<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}


    <style>
        body {
            font-family: 'Lato';
        }

        .precss_back {
          background-image: url('img/precss_back.png');
          background-repeat: repeat-x;
          background-color: #eee;
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0px;
          left: 0px;
          z-index: 9999;
        }

        .fa-btn {
            margin-right: 6px;
        }
        .dropdown-menu input[type="submit"] {
          width: 100%;
          background: white;
          border: none;
          text-align: left;
          padding: 3px 20px;
        }
        .dropdown-menu input[type="submit"]:hover {
          color: #262626;
          background-color: #f5f5f5;
        }
    </style>
</head>
<body id="app-layout">
  <div class="precss_back"></div>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Nugamer.nl
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav level-1">
                    @foreach($categories as $category)
                      <li><a href="/{{$category->slug}}">{{$category->title}}</a></li>
                    @endforeach
                </ul>
                <form class="navbar-form navbar-right search" method="GET" action="/search">
                  <div class="form-group">
                    <input type="text" class="form-control" name="search" placeholder="Zoek..." value="@if(isset($search)) {{ $search }} @endif">
                  </div>
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </form>
                <ul class="nav navbar-nav level-2">
                    @foreach($featured_categories as $category)
                      <li><a href="/{{$category->slug}}">{{$category->title}}</a></li>
                    @endforeach
                </ul>
                <ul class="nav navbar-nav level-3">
                  <li><a href="/?type=latest"><i class="fa fa-clock-o"></i> Latest</a></li>
                  <li><a href="/?type=trending"><i class="fa fa-arrow-up"></i> Trending</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    {!!Html::style('css/all.css')!!}

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    {!!Html::script('js/bootstrap.min.js')!!}

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-81732155-1', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
