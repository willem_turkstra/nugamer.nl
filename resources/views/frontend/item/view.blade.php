@extends('frontend.site')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="article-detail">
				<ol class="breadcrumb">
					<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/"><span itemprop="title">Home</span></a></li>
					@if(Session::has('breadcrumb_category'))
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/{{ Session::get('breadcrumb_category')->slug }}"><span itemprop="title">{{ Session::get('breadcrumb_category')->title }}</span></a></li>
					@endif
					<li><span>{{ $item->title }}</span></li>
				</ol>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					@if(!empty($item->meta_data->keyBy('key')['image']))
					<div class="article-image" style="background-image: url({{ $item->meta_data->keyBy('key')['image']->value}});"></div>
					@else
					<img class="article-image" style="background-image: url(http://placehold.it/400x300)" />
					@endif
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<h1>{{$item->title}}</h1>
					<div class="description">{!! $item->description !!}</div>
					<a href="/item/{{$item->id}}/leave" class="btn btn-default btn-theme" target="_blank">Lees meer op {{$item->feed->title}} &gt;&gt;</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div id="disqus_thread"></div>
			<script>
			var disqus_config = function () {
			    this.page.url = 'http://dragle.nl/item/{{$item->id}}';  // Replace PAGE_URL with your page's canonical URL variable
			    this.page.identifier = '{{$item->id}}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
			};

			(function() { // DON'T EDIT BELOW THIS LINE
			    var d = document, s = d.createElement('script');
			    s.src = '//dragle-nl.disqus.com/embed.js';
			    s.setAttribute('data-timestamp', +new Date());
			    (d.head || d.body).appendChild(s);
			})();
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
		</div>
	</div>
</div>
@endsection
