@extends('frontend.site')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="article-detail">
				<ol class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li>{{ $category->title }}</li>
				</ol>
				<div class="row category-detail">
					@if(!empty($category->image))
					<div class="col-md-6">
						<img src="/uploads/categories/{{$category->image}}" />
					</div>
					@endif
					<div class="@if(!empty($category->image)) col-md-6 @else col-md-12 @endif category-kop">
						<h1>{{ $category->title }}</h1>
						<p>{!! $category->description !!}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		@foreach($feed_items as $item)
			@include('frontend/components/grid_feed_item')
		@endforeach
	</div>
	<div class="row">
		<div class="col-md-12">
			{{ $feed_items->render() }}
		</div>
	</div>
</div>
@endsection
