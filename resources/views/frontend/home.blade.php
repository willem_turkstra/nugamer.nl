@extends('frontend.site')

@section('content')
<div class="container">
	<div class="row">
		@foreach($feed_items as $item)
			@include('frontend/components/grid_feed_item')
		@endforeach
	</div>
</div>
@endsection
