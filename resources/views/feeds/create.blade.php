@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
			<div class="card-header"><div class="card-title"><div class="title">Feeds</div></div></div>
				<div class="card-body">
          {!! Form::model($feed, ['route' => ['admin.feeds.store'], 'class' => 'form-horizontal']) !!}
              @include('feeds/partials/_form', ['submit_text' => 'Create feed'])
          {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
