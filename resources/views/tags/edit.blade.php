@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
			<div class="card-header"><div class="card-title"><div class="title">Tags</div></div></div>
			<div class="card-body">
        {!! Form::model($tag, ['method' => 'PATCH', 'route' => ['admin.tags.update', $tag->id], 'class' => 'form-horizontal']) !!}
            @include('tags/partials/_form', ['submit_text' => 'Update tag'])
        {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
