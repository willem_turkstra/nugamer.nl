<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		<div class="col-md-offset-4 col-md-6">
			@if(!empty($synonym))
				<input type="hidden" name="synonym[{{$synonym->id}}][id]" value="{{$synonym->id}}" />
				<input id="title" type="text" class="form-control" name="synonym[{{$synonym->id}}][title]"  value="{{$synonym->title}}" />
			@else
				<input id="title" type="text" class="form-control" name="synonym[][title]"  ) />
			@endif
		</div>
		<div class="col-md-2">
			<div class="btn-group">
				<span class="btn btn-default tag-add-synonym"><i class="fa fa-plus"></i></span>
				<span class="btn btn-default tag-remove-synonym" @if(!empty($synonym)) data-tag-id="{{$synonym->id}}" @endif><i class="fa fa-remove"></i></span>
			</div>
		</div>
</div>
