
{{ csrf_field() }}
<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		<label for="title" class="col-md-4 control-label">Title</label>
		<div class="col-md-6">
				<input id="title" type="text" class="form-control" name="title" value="{{ $tag->title }}">
				@if ($errors->has('title'))
						<span class="help-block">
								<strong>{{ $errors->first('title') }}</strong>
						</span>
				@endif
		</div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
		<label for="status" class="col-md-4 control-label">Status</label>
		<div class="col-md-6">
				<select id="status" name="status" class="selectpicker">
					<option @if ($tag->status == 'A')selected @endif>Active</option>
					<option @if ($tag->status == 'D')selected @endif>Disabled</option>
				</select>
				@if ($errors->has('status'))
						<span class="help-block">
								<strong>{{ $errors->first('status') }}</strong>
						</span>
				@endif
		</div>
</div>
<div class="col-md-6 col-md-offset-4">
	<h3>Synonyms</h3>
</div>
<div id="synonym_form_container">
	@if(!$tag->synonyms->isEmpty())

		@foreach($tag->synonyms as $synonym)
			@include('tags/partials/_synonym', ['synonym' => $synonym])
		@endforeach
	@else
			@include('tags/partials/_synonym')
	@endif
</div>
<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary">
						<i class="fa fa-btn fa-save"></i> Save
				</button>
		</div>
</div>
