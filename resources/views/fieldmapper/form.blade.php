@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
    	<div class="panel panel-default">
				<div class="panel-heading">Fieldmapper</div>
					<div class="panel-body">
						<!--{!! Form::model($feed, ['method' => 'PATCH', 'route' => ['admin.fieldmap.update', 'id' => $feed->id], 'class' => 'form-horizontal']) !!}-->
                {{ csrf_field() }}
								<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
														<i class="fa fa-btn fa-save"></i> Save
												</button>
										</div>
								</div>
            <!--{!! Form::close() !!}-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
